# Community Cage Reverse DNS

This repository holds the reverse zone for the Red Hat Community Cage IP range that is then published using [DNS4Tenants](https://osci.io/offers/dns4tenants/). Housed tenants are able to update their reverse by themselves.
